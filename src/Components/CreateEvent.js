import React, { Component } from "react";
import fire from "./Firebase";
import { Link } from "react-router-dom";
export class CreateEvent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      eventName: "",
      desc: "",
      duration: "",
      location: "",
      maxParticipants: "",
      fees: "",
      createdBy: localStorage.getItem("userId"),
      requirements: [],
      requirement: ""
    };
  }
  submitHandler() {
    const db = fire.firestore();
    db.collection("events")
      .add(this.state)
      .then(() => {
        this.props.history.push("/eventslist");
      });
  }
  addReq() {
    this.setState({
      requirements: [...this.state.requirements, this.state.requirement],
      requirement: ""
    });
  }
  deleteReq(data) {
    this.setState({
      requirements: this.state.requirements.filter(hello => {
        return data !== hello;
      })
    });
  }
  render() {
    return (
      <div>
        <Link to="/eventslist">Go Back</Link>
        <h1>Create Event</h1>

        <input
          type="text"
          value={this.state.eventName}
          onChange={eventName =>
            this.setState({ eventName: eventName.target.value })
          }
        />
        <input
          type="text"
          value={this.state.desc}
          onChange={desc => this.setState({ desc: desc.target.value })}
        />
        <input
          type="text"
          value={this.state.duration}
          onChange={duration =>
            this.setState({ duration: duration.target.value })
          }
        />
        <input
          type="text"
          value={this.state.location}
          onChange={location =>
            this.setState({ location: location.target.value })
          }
        />
        <input
          type="text"
          value={this.state.maxParticipants}
          onChange={maxParticipants =>
            this.setState({ maxParticipants: maxParticipants.target.value })
          }
        />
        <input
          type="text"
          value={this.state.fees}
          onChange={fees => this.setState({ fees: fees.target.value })}
        />
        <input
          type="text"
          value={this.state.requirement}
          onChange={e => this.setState({ requirement: e.target.value })}
        />
        <button onClick={() => this.addReq()}>Add Requirement</button>
        {this.state.requirements.map((data, i) => {
          return (
            <li key={i}>
              {data} <a onClick={() => this.deleteReq(data)}>Delete</a>
            </li>
          );
        })}
        <button onClick={() => this.submitHandler()}>Submit</button>
      </div>
    );
  }
}

export default CreateEvent;

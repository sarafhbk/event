import React, { Component } from 'react'

export class Employee extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         color: this.props.color
      }
    }

  render() {
    return (
      <div>
        <li style={{'color': this.props.active ? 'blue': 'black' }} >{this.props.emp}</li>
      </div>
    )
  }
}

export default Employee

import React, { Component } from 'react'
import Employeelist from './Employeelist'
export class Sample extends Component {
    constructor(props) {
        super(props)
        this.state = {
fetchedArray : [],
load: true,
inputVal: '',
clickedEmp: '',
passedEmployee: '',
        }
      
      
      }
      componentDidMount() {
         fetch('http://dummy.restapiexample.com/api/v1/employees').then((data) => {
             if(data) {
                 this.setState({
                     load: !this.state.load
                 })
             }
return data.json().then((datum)=> {
    
   this.setState({
       fetchedArray: datum
   })
    })
          })
      }
      submitHandler() {
          this.setState({
              fetchedArray: [{employee_name: this.state.inputVal }, ...this.state.fetchedArray]
          })
      }
      handleSubmit = (data) => {
          this.setState({
              passedEmployee: data
          })
      }
  render() {

    return (
<div>
<input value={this.state.inputVal} onChange={(e)=> this.setState({inputVal: e.target.value}) } />
<button onClick={()=> this.submitHandler()} >Submit</button>
<p>The Clicked Employee is {this.state.passedEmployee}</p>
<Employeelist data = {this.state.fetchedArray} open ={this.handleSubmit} />

</div>
    )
  }
}



export default Sample

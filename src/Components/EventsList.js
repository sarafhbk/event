import React, { Component } from "react";
import Auth from "./Auth";
import Event from "./Event";
import fire from "./Firebase";

export class EventsList extends Component {
  constructor(props) {
    super(props);
    this.db = fire.firestore();
    this.ref = fire.firestore().collection("events");
    this.notifyRef = fire
      .firestore()
      .collection("users")
      .doc(localStorage.getItem("userId"))
      .collection("notification");

    this.state = {
      events: [],
      notifications: []
    };
  }

  logout() {
    localStorage.removeItem("userId");
    Auth.authenticated = false;
    this.props.history.push("/");
  }
  onCollectionUpdate = querySnapshot => {
    const events = [];
    querySnapshot.forEach(doc => {
      const {
        eventName,
        desc,
        duration,
        location,
        fees,
        maxParticipants,
        createdBy
      } = doc.data();
      events.push({
        key: doc.id,
        doc,
        eventName,
        desc,
        duration,
        location,
        fees,
        maxParticipants,
        createdBy
      });
    });
    this.setState({
      events
    });
  };

  notifyCollection = querySnapshot => {
    const notifications = [];
    querySnapshot.forEach(doc => {
      const { name } = doc.data();
      notifications.push({
        key: doc.id,
        name
      });
    });
    this.setState({
      notifications
    });
    // console.log(this.state.notifications);
  };

  componentDidMount() {
    this.ref.onSnapshot(this.onCollectionUpdate);
    this.notifyRef.onSnapshot(this.notifyCollection);
  }

  deleteEvent(key, eventName) {
    this.setState({
      events: this.state.events.filter(data => {
        return data.key !== key;
      })
    });
    const db = fire.firestore();
    db.collection("events")
      .doc(key)
      .collection("participants")
      .onSnapshot(data => {
        data.forEach(doc => {
          if (doc.exists) {
            db.collection("users")
              .doc(doc.id)
              .collection("notification")
              .add({
                name: eventName
              });
          }
        });
      });
    db.collection("events")
      .doc(key)
      .delete()
      .then(() => {
        console.log("Document successfully deleted!");
      })
      .catch(error => {
        console.error("Error removing document: ", error);
      });
  }
  deleteNotification(notify) {
    this.setState({
      notifications: this.state.notifications.filter(data => {
        return data.name !== notify.name;
      })
    });
    this.db
      .collection("users")
      .doc(localStorage.getItem("userId"))
      .collection("notification")
      .doc(notify.key)
      .delete();
  }
  participate(key) {
    this.ref
      .doc(key)
      .get()
      .then(doc => {
        const data = { ...doc.data(), docId: doc.id };
        this.props.history.push(`/requirements/:${JSON.stringify(data)}`);
      });
  }
  render() {
    console.log(this.state.notifications);
    return (
      <div>
        <h1>Events List</h1>
        <button onClick={() => this.props.history.push("/createevent")}>
          Add New Event
        </button>
        {this.state.events.map(event => (
          <Event
            key={event.key}
            id={event.doc.id}
            eventName={event.eventName}
            desc={event.desc}
            duration={event.duration}
            location={event.location}
            fees={event.fees}
            tags={event.tags}
            maxParticipants={event.maxParticipants}
            createdBy={event.createdBy}
            requirements={event.requirements}
            delete={this.deleteEvent.bind(this, event.doc.id, event.eventName)}
            participate={this.participate.bind(this, event.doc.id)}
          />
        ))}

        <button onClick={() => this.logout()}>Logout</button>

        <h2>Notifications</h2>
        {this.state.notifications.map((notify, i) => (
          <li key={i}>
            Sorry! The Event {notify.name} was removed{" "}
            <a onClick={() => this.deleteNotification(notify)}>delete</a>
          </li>
        ))}
      </div>
    );
  }
}

export default EventsList;

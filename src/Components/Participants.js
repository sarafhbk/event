import React, { Component } from "react";
import fire from "./Firebase";
import { Link } from "react-router-dom";

export class Participants extends Component {
  constructor(props) {
    super(props);
    this.ref = fire.firestore().collection("events");

    this.state = {
      participantsKey: [],
      participantsValue: [],
      docData: []
    };
  }
  onCollectionUpdate = querySnapshot => {
    querySnapshot.forEach(doc => {
      if (doc.data().createdBy === localStorage.getItem("userId")) {
        this.ref
          .doc(doc.id)
          .collection("participants")
          .onSnapshot(data => {
            data.forEach(doc => {
              this.state.docData.push(doc.data());
              this.setState({
                participantsKey: [...doc.data().key],
                participantsValue: [...doc.data().values]
              });
            });
          });
      }
    });
  };
  componentDidMount() {
    this.ref.onSnapshot(this.onCollectionUpdate);
  }
  render() {
    // console.log(this.state.docData);
    return (
      <div>
        <h1>Participants List</h1>
        <Link to="/eventslist">Go Back</Link>
        {/* {
this.state.participantsKey.map((data, i)=>
   <li key={i} >{data}
   {
       <a>{this.state.participantsValue[i]}</a>

   }
   </li>
   
)
        } */}

        {this.state.docData.map((data, i) => (
          <div key={i} style={{ paddingTop: "20px" }}>
            {data.key.map((doc, j) => (
              <div key={j}>
                <b>{doc}</b>&nbsp;
                {<a>{data.values[j]}</a>}
              </div>
            ))}
          </div>
        ))}
      </div>
    );
  }
}

export default Participants;

import React, { Component } from "react";
import Modal from "react-modal";
import { Link } from "react-router-dom";
import fire from "./Firebase";
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)"
  }
};

Modal.setAppElement("#root");
export class Event extends Component {
  constructor(props) {
    super();
    this.state = {
      modalIsOpen: false,
      id: props.id,
      eventName: props.eventName,
      desc: props.desc,
      duration: props.duration,
      location: props.location,
      fees: props.fees,
      maxParticipants: props.maxParticipants,
      createdBy: props.createdBy,
      requirements: props.requirements,
      participated: []
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  openModal() {
    this.setState({ modalIsOpen: true });
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }
  participate() {
    console.log();
  }
  componentDidMount() {
    fire
      .firestore()
      .collection("users")
      .doc(localStorage.getItem("userId"))
      .collection("participated events")
      .onSnapshot(docs => {
        docs.forEach(doc => {
          if (doc.data().docId === this.state.id) {
            // console.log(doc.data().docId);
            this.setState({
              participated: doc.data().docId
            });
          }
        });
      });
  }
  render() {
    return (
      <div>
        <li>
          <a onClick={this.openModal}>{this.state.eventName}</a>
        </li>
        {this.state.createdBy === localStorage.getItem("userId") ? (
          <>
            <Link to={`/updateevent/${JSON.stringify(this.state)}`}>Edit</Link>
            <a onClick={this.props.delete}>Delete</a>{" "}
            <Link to="/participants">Participants</Link>
          </>
        ) : null}
        {this.state.createdBy === localStorage.getItem("userId") ||
        this.state.participated === this.props.id ? null : (
          <button onClick={this.props.participate}>Participate</button>
        )}

        {this.state.participated === this.props.id ? <i>Participated</i> : null}

        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <h2 ref={title => (this.title = title)}>{this.state.eventName}</h2>
          <p>Description: {this.state.desc}</p>
          <p>Duration: {this.state.duration}</p>
          <p>Location: {this.state.location}</p>
          <p>Fees: {this.state.fees}</p>
          <p>Max Members: {this.state.maxParticipants}</p>
          <button onClick={this.closeModal}>Close</button>
        </Modal>
      </div>
    );
  }
}

export default Event;

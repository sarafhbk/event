import React, { Component } from "react";
import fire from "./Firebase";

export class UpdateEvent extends Component {
  constructor() {
    super();
    this.value = this.state = {
      id: "",
      eventName: "",
      desc: "",
      duration: "",
      location: "",
      maxParticipants: "",
      fees: ""
    };
  }

  componentWillMount() {
    // console.log(JSON.parse(this.props.match.params.id));
    const {
      id,
      eventName,
      desc,
      duration,
      location,
      maxParticipants,
      fees
    } = JSON.parse(this.props.match.params.id);
    this.setState({
      id,
      eventName,
      desc,
      duration,
      location,
      maxParticipants,
      fees
    });
  }
  onSubmitHandler(key) {
    const db = fire.firestore();
    db.collection("events")
      .doc(key)
      .update(this.state)
      .then(() => {
        this.props.history.push("/eventslist");
        console.log("updated Successfully");
      });
  }
  render() {
    // console.log(this.state.requirements);
    return (
      <div>
        <h1>Update Event</h1>
        {/* <p>{console.log(this.props.match.params.id)}</p> */}

        <input
          type="text"
          value={this.state.eventName}
          onChange={e => this.setState({ eventName: e.target.value })}
        />
        <input
          type="text"
          value={this.state.desc}
          onChange={e => this.setState({ desc: e.target.value })}
        />
        <input
          type="text"
          value={this.state.duration}
          onChange={e => this.setState({ duration: e.target.value })}
        />
        <input
          type="text"
          value={this.state.location}
          onChange={e => this.setState({ location: e.target.value })}
        />
        <input
          type="text"
          value={this.state.maxParticipants}
          onChange={e => this.setState({ maxParticipants: e.target.value })}
        />
        <input
          type="text"
          value={this.state.fees}
          onChange={e => this.setState({ fees: e.target.value })}
        />

        {/* {this.state.requirements.map((data, i) => (
          <div style={{ paddingTop: "20px" }}>
            {data.key.map((doc, j) => (
              <div key={j}>
                <input value={data.values[j]} placeholder={doc} />
              </div>
            ))}
          </div>
        ))} */}

        <button onClick={() => this.onSubmitHandler(this.state.id)}>
          Update
        </button>
        {/* <p>{this.value.eventName}</p> */}
      </div>
    );
  }
}

export default UpdateEvent;

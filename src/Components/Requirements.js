import React, { Component } from "react";
import fire from "./Firebase";
import { Link } from "react-router-dom";
export class Requirements extends Component {
  constructor(props) {
    super(props);
    this.stringify = null;
    this.state = {
      requirements: [],
      values: []
    };
  }
  componentWillMount() {
    const json = this.props.match.params.data.replace(":", "");
    // console.log(json);

    this.stringify = JSON.parse(json);
    console.log(this.stringify);
    // console.log(this.stringify);
    this.setState({
      requirements: [...this.stringify.requirements]
    });
    console.log(this.stringify.docId);
  }
  submitHandler() {
    const db = fire.firestore();
    db.collection("events")
      .doc(this.stringify.docId)
      .collection("participants")
      .doc(localStorage.getItem("userId"))
      .set({
        key: this.state.requirements,
        values: this.state.values
      });

    db.collection("users")
      .doc(localStorage.getItem("userId"))
      .collection("participated events")
      .doc(this.stringify.docId)
      .set(this.stringify)
      .then(() => {
        this.props.history.push("/eventslist");
      });
    // console.log(this.state.values);
  }
  createUI() {
    return this.state.requirements.map((el, i) => (
      <div key={i}>
        <input
          type="text"
          placeholder={el}
          onChange={this.handleChange.bind(this, i)}
        />
      </div>
    ));
  }
  handleChange(i, event) {
    let values = [...this.state.values];
    values[i] = event.target.value;
    this.setState({ values });
  }
  render() {
    // console.log(this.props);

    return (
      <div>
        {this.createUI()}
        <button onClick={() => this.submitHandler()}>Submit</button>
        <Link to="/eventslist">Back</Link>
      </div>
    );
  }
}

export default Requirements;

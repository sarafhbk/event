import React, { Component } from 'react'
import Employee from './Employee';
export class Employeelist extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         index: -1 ,
      }
    }
    
    submit(data, i) {
        this.props.open(data);
this.setState({
    index: i
})
    }
  render() {
    return (
      <div>
              {
 this.props.data === 0 ? <h2>LOADING...</h2> : this.props.data.map((data, i)=>
 <div key={i}  onClick={() => this.submit(data.employee_name, i)} >
 <Employee emp = { data.employee_name} active={this.state.index === i} />
 </div>

            )
      }
      </div>
    )
  }
}

export default Employeelist
